package com.epam.rd.java.basic.task7.db;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.*;
import java.util.*;


import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

    private static final String APP_PROPS_FILE = "app.properties";
    private static String FULL_URL;

    static {
        try {
            FULL_URL = Files.readString(Path.of(APP_PROPS_FILE)).replace("connection.url=", "");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static final String SELECT_FROM_USERS = "SELECT * FROM users";
    public static final String SELECT_FROM_TEAMS = "SELECT * FROM teams";
    public static final String SELECT_USER_BY_LOGIN = "SELECT id, login FROM users WHERE login = ?";
    public static final String SELECT_TEAM_BY_NAME = "SELECT id, name FROM teams WHERE name = ?";
    public static final String SELECT_TEAM_BY_ID = "SELECT id, name FROM teams WHERE id = ?";
    public static final String SELECT_TEAMS_BY_USER = "SELECT team_id FROM users_teams WHERE user_id = ?";
    public static final String INSERT_INTO_USERS = "INSERT INTO users VALUES (DEFAULT, ?)";
    public static final String INSERT_INTO_TEAMS = "INSERT INTO teams VALUES (DEFAULT, ?)";
    public static final String INSERT_INTO_USERS_TEAMS = "INSERT INTO users_teams (user_id, team_id) VALUES (?, ?)";
    public static final String DELETE_TEAM = "DELETE FROM teams WHERE id = ?";
    public static final String DELETE_USER = "DELETE FROM users WHERE id = ?";
    public static final String UPDATE_TEAM = "UPDATE teams SET name = ? WHERE id = ?";


    private static DBManager instance;

    private DBManager() {
        // hide
    }

    public static synchronized DBManager getInstance() {
        if (DBManager.instance == null) {
            DBManager.instance = new DBManager();
        }
        return DBManager.instance;
    }

    public List<User> findAllUsers() throws DBException {
        List<User> users = new ArrayList<>();
        try (Connection con = DriverManager.getConnection(FULL_URL);
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery(SELECT_FROM_USERS)) {
            while (rs.next()) {
                mapUser(users, rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("", e);
        }
        return users;
    }

    private void mapUser(List<User> users, ResultSet rs) throws SQLException {
        User u = new User();
        u.setId(rs.getInt("id"));
        u.setLogin(rs.getString("login"));
        users.add(u);
    }

    public boolean insertUser(User user) throws DBException {
        Connection con = null;
        PreparedStatement stmt = null;

        try {
            con = DriverManager.getConnection(FULL_URL);
            stmt = con.prepareStatement(INSERT_INTO_USERS, Statement.RETURN_GENERATED_KEYS);
            int k = 0;
            stmt.setString(++k, user.getLogin());

            int count = stmt.executeUpdate();
            if (count > 0) {
                try (ResultSet rs = stmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        user.setId(rs.getInt(1));
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("", e);
        } finally {
            close(con);
            close(stmt);
        }
        return true;
    }

    private void close(AutoCloseable stmt) {
        if (stmt != null) {
            try {
                stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public boolean deleteUsers(User... users) throws DBException {
        try (Connection con = DriverManager.getConnection(FULL_URL);
             PreparedStatement stmt = con.prepareStatement(DELETE_USER)) {

            for (User user : users) {
                int k = 0;
                stmt.setInt(++k, user.getId());
                stmt.executeUpdate();
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("", e);
        }
        return true;
    }

    public User getUser(String login) throws DBException {
        User u = new User();

        try (Connection con = DriverManager.getConnection(FULL_URL);
             PreparedStatement stmt = con.prepareStatement(SELECT_USER_BY_LOGIN)) {
            int k = 0;
            stmt.setString(++k, login);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                u.setId(rs.getInt("id"));
                u.setLogin(rs.getString("login"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("", e);
        }
        return u;
    }

    public Team getTeam(String name) throws DBException {
        Team t = new Team();

        try (Connection con = DriverManager.getConnection(FULL_URL);
             PreparedStatement stmt = con.prepareStatement(SELECT_TEAM_BY_NAME)) {
            int k = 0;
            stmt.setString(++k, name);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                t.setId(rs.getInt("id"));
                t.setName(rs.getString("name"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("", e);
        }
        return t;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teams = new ArrayList<>();
        try (Connection con = DriverManager.getConnection(FULL_URL);
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery(SELECT_FROM_TEAMS)) {
            while (rs.next()) {
                mapTeam(teams, rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("123", e);
        }
        return teams;
    }

    private void mapTeam(List<Team> teams, ResultSet rs) throws SQLException {
        Team t = new Team();
        t.setId(rs.getInt("id"));
        t.setName(rs.getString("name"));
        teams.add(t);
    }

    public boolean insertTeam(Team team) throws DBException {
        Connection con = null;
        PreparedStatement stmt = null;

        try {
            con = DriverManager.getConnection(FULL_URL);
            stmt = con.prepareStatement(INSERT_INTO_TEAMS, Statement.RETURN_GENERATED_KEYS);
            int k = 0;
            stmt.setString(++k, team.getName());

            int count = stmt.executeUpdate();
            if (count > 0) {
                try (ResultSet rs = stmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        team.setId(rs.getInt(1));
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("", e);
        } finally {
            close(con);
            close(stmt);
        }
        return true;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        Connection con = null;
        PreparedStatement stmt = null;
        try {
            con = DriverManager.getConnection(FULL_URL);
            con.setAutoCommit(false);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);


            for (Team team : teams) {
                stmt = con.prepareStatement(INSERT_INTO_USERS_TEAMS);

                int k = 0;
                stmt.setInt(++k, user.getId());
                stmt.setInt(++k, team.getId());

                stmt.executeUpdate();
            }

            con.commit();

        } catch (SQLException e) {
            e.printStackTrace();
            rollback(con);
            throw new DBException("", e);
        } finally {
            autoCommit(con);
            close(con);
            close(stmt);
        }
        return true;
    }

    private void rollback(Connection con) {
        try {
            if (con != null) {
                con.rollback();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void autoCommit(Connection con) {
        try {
            if (con != null) {
                con.setAutoCommit(true);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> teams = new ArrayList<>();
        try (Connection con = DriverManager.getConnection(FULL_URL);
             PreparedStatement stmt = con.prepareStatement(SELECT_TEAMS_BY_USER)) {

            int k = 0;
            stmt.setInt(++k, user.getId());

            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                teams.add(getTeamById(rs.getInt("team_id")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("", e);
        }
        return teams;
    }

    private Team getTeamById(int team_id) throws DBException {
        Team t = new Team();

        try (Connection con = DriverManager.getConnection(FULL_URL);
             PreparedStatement stmt = con.prepareStatement(SELECT_TEAM_BY_ID)) {
            int k = 0;
            stmt.setInt(++k, team_id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                t.setId(rs.getInt("id"));
                t.setName(rs.getString("name"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("", e);
        }
        return t;
    }

    public boolean deleteTeam(Team team) throws DBException {
        try (Connection con = DriverManager.getConnection(FULL_URL);
             PreparedStatement stmt = con.prepareStatement(DELETE_TEAM)) {

            int k = 0;
            stmt.setInt(++k, team.getId());
            stmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("", e);
        }
        return true;
    }

    public boolean updateTeam(Team team) throws DBException {
        try (Connection con = DriverManager.getConnection(FULL_URL);
             PreparedStatement stmt = con.prepareStatement(UPDATE_TEAM)) {
            int k = 0;
            stmt.setString(++k, team.getName());
            stmt.setInt(++k, team.getId());
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("", e);
        }
        return true;
    }
}
